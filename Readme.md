How to run the app


In order to run this app, follows these instructions:

1. Download the repository in one folder (no license).
2. Install MongoDB you can find the instructions in this link: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
3. Install nodeJs, you can find the instructions in this link: https://nodejs.org/es/download/
4. Create a DB following th instructios given in this page: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
    In the section: 'Run MongoDB Community Edition from the Command Interpreter'
5. In a terminal go to the folder backend and run this command: npm install
6. After that, go o the folder frontend and run again the command npm install
7. Go back to the backend folder and run the command: nodemon app.js, should appear the following messages:
    "Server is on port: 3000"
    "Connected to DB"
8. Go to the folder frontend and run the following command: ng serve
9. Will appear the message "Compilation completed"
And finally in your favourite browser in the address bar write: "localhost:4200" and press enter, and the app will be runnning.
