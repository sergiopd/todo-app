const express = require("express");

/* Initial */
const app = express();
const { mongoose } = require("./db/mongoose");
// Load mongoose models
// const { List, Todo } = require("./db/models");
const { List, Todo } = require("./db/models/");

/* Settings */
app.set("port", process.env.PORT || 3000);
app.use(express.json());

/* Middlewares */
// CORS Header
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, x-access-token, x-refresh-token, _id"
  );

  res.header(
    "Access-Control-Expose-Headers",
    "x-access-token, x-refresh-token"
  );

  next();
});

/* Routes */
// List routes
// All info will be passed throw JSON request body
app.get("/lists", (req, res) => {
  // Get an array of all the lists in DB
  List.find({}).then((lists) => {
    res.send(lists);
  });
});

app.post("/lists", (req, res) => {
  // Create a new list and return it to user (incl id)
  let title = req.body.title;
  let newList = new List({
    title,
  });
  newList.save().then((listDoc) => {
    // Full list doc is returned (incl. ID)
    res.send(listDoc);
  });
});

app.patch("/lists/:id", (req, res) => {
  // Update specific list (id in url)
  List.findOneAndUpdate({ _id: req.params.id }, { $set: req.body }).then(() => {
    res.send({ message: "List edited succesfully!" });
  });
});

app.delete("/lists/:id", (req, res) => {
  // Delete specific list (id in url)
  List.findOneAndRemove({ _id: req.params.id }).then((removedListDocument) => {
    res.send(removedListDocument);
  });
});

// -Todos routes
app.get("/lists/:listId/todos", (req, res) => {
  // Get an array of all the todos in a specific list
  Todo.find({
    _listId: req.params.listId,
  }).then((todos) => {
    res.send(todos);
  });
});

app.get("/lists/:listId/todos/:todoId", (req, res) => {
  Todo.find({
    _id: req.params.todoId,
    _listId: req.params.listId,
  }).then((gettedTodoDocument) => {
    res.send(gettedTodoDocument);
  });
});

app.post("/lists/:listId/todos", (req, res) => {
  // Full list doc is returned (incl. ID)
  let newTodo = new Todo({
    title: req.body.title,
    _listId: req.params.listId,
  });
  newTodo.save().then((newTodoDoc) => {
    res.send(newTodoDoc);
  });
});

app.patch("/lists/:listId/todos/:todoId", (req, res) => {
  // Update specific todo in a specific list (id in url)
  Todo.findOneAndUpdate(
    {
      _id: req.params.todoId,
      _listId: req.params.listId,
    },
    { $set: req.body }
  ).then(() => {
    res.send({ message: "Updated succesfully!" });
  });
});

app.delete("/lists/:listId/todos/:todoId", (req, res) => {
  Todo.findOneAndRemove({
    _id: req.params.todoId,
    _listId: req.params.listId,
  }).then((removedTodoDocument) => {
    res.send(removedTodoDocument);
  });
});

/* Static */

/* Server init */
app.listen(app.get("port"), () => {
  console.log("Server is on port: ", app.get("port"));
});
