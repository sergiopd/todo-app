const { List } = require("./list.model");
const { Todo } = require("./todo.model");

module.exports = {
  List,
  Todo,
};
