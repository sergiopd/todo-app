/* Logic connection to MongoDB */

//Import the mongoose module
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

//Set up default mongoose connection
mongoose
  .connect("mongodb://localhost:27017/todo-db-app", {
    useNewUrlParser: true,
  })
  .then(() => console.log("Connected to DB"))
  .catch((err) => {
    console.log("Error in MongoDB connection");
    console.log(err);
  });

module.exports = { mongoose };
