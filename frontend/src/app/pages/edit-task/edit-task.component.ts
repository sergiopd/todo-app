import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TodoService } from 'src/app/todo.service';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';

export class Input {
  public todoName: string;
}
@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.scss'],
})
export class EditTaskComponent implements OnInit {
  constructor(
    private todoService: TodoService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  listId: string;
  todoId: string;
  editTodo: FormGroup;

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.todoId = params.todoId;
      this.listId = params.listId;
    });

    this.editTodo = this.formBuilder.group({
      todoname: ['', Validators.required],
    });
  }

  model = new Input();

  onEditTodo(title: string) {
    this.todoService.editTodo(this.listId, this.todoId, title).subscribe(() => {
      this.router.navigate(['/lists', this.listId]);
    });
  }
}
