import { Component, OnInit } from '@angular/core';
import { TodoService } from 'src/app/todo.service';
import { Router } from '@angular/router';
import { List } from 'src/app/models/list.model';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';

export class Input {
  public listName: string;
}

@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.scss'],
})
export class NewListComponent implements OnInit {
  constructor(
    private todoService: TodoService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  createList: FormGroup;

  ngOnInit(): void {
    this.createList = this.formBuilder.group({
      listname: ['', Validators.required],
    });
  }

  model = new Input();

  createNewList(title: string) {
    this.todoService.createNewList(title).subscribe((list: any) => {
      // Drive user to the List created /lists/list._id
      this.router.navigate(['/lists', list._id]);
    });
  }
}
