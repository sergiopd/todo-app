import { Component, OnInit } from '@angular/core';
import { TodoService } from 'src/app/todo.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Todo } from 'src/app/models/todo.model';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';

export class Input {
  public todoName: string;
}

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss'],
})
export class NewTaskComponent implements OnInit {
  listId: string;

  constructor(
    private todoService: TodoService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  createTask: FormGroup;

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.listId = params['listId'];
    });

    this.createTask = this.formBuilder.group({
      todoname: ['', Validators.required],
    });
  }

  model = new Input();

  createNewTodo(title: string) {
    // Send web request to create a new todo
    this.todoService
      .createNewTodo(title, this.listId)
      .subscribe((newTodo: any) => {
        // Drive user to the list where todo was created
        this.router.navigate(['../'], { relativeTo: this.route });
      });
  }
}
