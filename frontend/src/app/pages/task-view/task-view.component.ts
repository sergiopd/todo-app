import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { List } from 'src/app/models/list.model';
import { Todo } from 'src/app/models/todo.model';
import { TodoService } from 'src/app/todo.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss'],
})
export class TaskViewComponent implements OnInit {
  lists: List[];
  todos: Todo[];
  selectedListId: string;

  constructor(
    private todoService: TodoService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  changeClass: boolean;

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      if (params.listId) {
        this.selectedListId = params.listId;
        this.todoService.getTodos(params.listId).subscribe((todos: Todo[]) => {
          this.todos = todos;
        });
      } else {
        this.todos = undefined;
      }
    });

    this.todoService.getLists().subscribe((lists: List[]) => {
      this.lists = lists;
    });
  }

  onDeleteList() {
    this.todoService.deleteList(this.selectedListId).subscribe(() => {
      this.router.navigate(['/lists']);
    });
  }

  onTodoClik(todo: Todo) {
    // Mark as completed the to do item
    this.todoService.complete(todo).subscribe(() => {
      todo.completed = !todo.completed;
    });
  }

  onDeleteTodo(id: string) {
    this.todoService
      .deleteTodo(this.selectedListId, id)
      .subscribe((res: any) => {
        this.todos = this.todos.filter((val) => val._id !== id);
      });
  }

  showMenu() {
    if (document.querySelector('.-show')) {
      this.changeClass = false;
    } else {
      this.changeClass = true;
    }
  }
}
