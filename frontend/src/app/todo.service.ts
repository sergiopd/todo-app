import { Injectable } from '@angular/core';
import { Todo } from './models/todo.model';
import { WebRequestService } from './web-request.service';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  constructor(private webReqService: WebRequestService) {}

  createNewList(title: string) {
    // Send web request to create a new list
    return this.webReqService.post('lists', { title });
  }

  deleteList(id: string) {
    return this.webReqService.delete(`lists/${id}`);
  }

  editList(id: string, title: string) {
    // Send web request to update a list
    return this.webReqService.patch(`lists/${id}`, { title });
  }

  getLists() {
    return this.webReqService.get('lists');
  }

  getTodos(listId: string) {
    return this.webReqService.get(`lists/${listId}/todos`);
  }

  createNewTodo(title: string, listId: string) {
    // Send web request to create a new todo
    return this.webReqService.post(`lists/${listId}/todos`, { title });
  }

  complete(todo: Todo) {
    return this.webReqService.patch(`lists/${todo._listId}/todos/${todo._id}`, {
      completed: !todo.completed,
    });
  }

  deleteTodo(listId: string, todoId: string) {
    return this.webReqService.delete(`lists/${listId}/todos/${todoId}`);
  }

  editTodo(listId: string, todoId: string, title: string) {
    // Send web request to update a todo item
    return this.webReqService.patch(`lists/${listId}/todos/${todoId}`, {
      title,
    });
  }
}
